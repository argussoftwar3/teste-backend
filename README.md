# Sobre

Este documento README tem como objetivo fornecer as informações necessárias para executar o projeto em ambiente de desenvolvimento.

# 🚨

- npm install
- npx sequelize-cli db:migrate
- npx sequelize-cli db:seed:all
- npm start

O usuário padrão é admin/admin