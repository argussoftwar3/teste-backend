'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Votes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Votes.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'NO ACTION'
      })
      Votes.belongsTo(models.Movie, {
        foreignKey: 'movieId',
        onDelete: 'NO ACTION'
      })
    }
  };
  Votes.init({
    vote: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    movieId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Votes',
  });
  return Votes;
};