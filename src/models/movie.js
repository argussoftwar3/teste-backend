'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Movie.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'NO ACTION'
      })
    }
  };
  Movie.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    director: DataTypes.STRING,
    genre: DataTypes.STRING,
    userId: DataTypes.NUMBER
  }, {
    sequelize,
    paranoid: true,
    modelName: 'Movie',
  });
  return Movie;
};