const jwt = require("jsonwebtoken");
const config = require("../config/jwt.config.js");
const db = require("../models");
const User = db.User;

verifyToken = (req, res, next) => {
    let token = req.headers["x-access-token"];
  
    if (!token) {
      return res.status(403).send({
        message: "Token inválido!"
      });
    }
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.status(401).send({
          message: "Forbidden!"
        });
      }
      req.userId = decoded.id;
      next();
    });
  };

isAdmin = (req, res, next) => {
    User.findByPk(req.userId).then(user => {

        if(user.type === 'admin') {
            next();
            return;
        }

        res.status(403).send({
            message: "Usuário sem acesso administrativo!"
        });

        return;
    });
};

const authJwt = {
    verifyToken: verifyToken,
    isAdmin: isAdmin,
};

module.exports = authJwt;