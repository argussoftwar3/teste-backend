const controllers = require('../controllers');
const { authJwt } = require("../middlewares");

module.exports = (app) => {

    //Auth
    app.post('/api/auth', controllers.auth.login);

    //Users
    app.get('/api/users', [authJwt.verifyToken, authJwt.isAdmin], controllers.user.list);
    app.post('/api/users', [authJwt.verifyToken, authJwt.isAdmin], controllers.user.createUser);
    app.put('/api/users/:id', [authJwt.verifyToken, authJwt.isAdmin], controllers.user.update);
    app.delete('/api/users/:id', [authJwt.verifyToken, authJwt.isAdmin], controllers.user.delete);
    app.post('/api/users/admin', [authJwt.verifyToken, authJwt.isAdmin], controllers.user.createAdmin);

    //Movies
    app.get('/api/movies', controllers.movie.list);
    app.post('/api/movies', [authJwt.verifyToken, authJwt.isAdmin], controllers.movie.create);

    //Votes
    app.post('/api/vote', [authJwt.verifyToken], controllers.vote.create);
}