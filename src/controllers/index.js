const user = require('./user');
const movie = require('./movie');
const vote = require('./vote');
const auth = require('./auth');

module.exports = {
    user,
    movie,
    vote,
    auth
}