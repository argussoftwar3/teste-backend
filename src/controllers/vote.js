const Sequelize = require('sequelize');
const Vote = require('../models').Votes;
const Op = Sequelize.Op;

module.exports = {
    async create(req, res) {

        const {vote, movieId} = req.body;

        return Vote.create({
            vote: vote,
            movieId: movieId,
            userId: req.userId
        })
        .then((vote) => res.status(201).send({
            id: vote.id,
            vote: vote.vote
        }))
        .catch((error) => res.status(400).send(error));
    },
}