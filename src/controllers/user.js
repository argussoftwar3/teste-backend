const bcrypt = require('bcryptjs');
const User = require('../models').User;

module.exports = {
    list(req, res) {
        User.findAll()
        .then((users) => res.send(users))
        .catch((error) => res.status(400).send(error));
    },
    async createAdmin(req, res) {

        const {name, password, login} = req.body;

        return User.create({
            name: name,
            password: await bcrypt.hash(password, 10),
            login: login,
            type: 'admin'
        })
        .then((user) => res.status(201).send({
            id: user.id,
            name: user.name,
            createdAt: user.createdAt
        }))
        .catch((error) => res.status(400).send(error));
    },
    async createUser(req, res) {
        const {name, password, login} = req.body;

        return User.create({
            name: name,
            password: await bcrypt.hash(password, 10),
            login: login,
            type: 'user'
        })
        .then((user) => res.status(201).send({
            id: user.id,
            name: user.name,
            createdAt: user.createdAt
        }))
        .catch((error) => res.status(400).send(error));
    },
    async update(req, res) {

        const {name, password} = req.body;

        return User.update({
            name: name,
            password: (password) ? await bcrypt.hash(password, 10) : password
        }, {
            where: {
                id: req.params.id,
            }
        })
        .then(() => res.status(204).send())
        .catch((error) => res.status(400).send(error));
    },
    async delete(req, res) {
        return User.update({
            deletedAt: new Date(),
        }, {
            where: {
                id: req.params.id,
            }
        })
        .then(() => res.status(204).send())
        .catch((error) => res.status(400).send(error));
    },
}