const Sequelize = require('sequelize');
const User = require('../models').User;
const Op = Sequelize.Op;
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const config = require("../config/jwt.config.js");

module.exports = {
    async login(req, res) {

        return User.findOne({
            where: {
                login: req.body.login
            }
        })
        .then((user) => {
            if(!user) {
                res.status(404).send({message: 'Invalid user'});
                return;
            }

            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Senha incorreta!"
                });
            }

            var token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 86400
            });

            res.status(200).send({
                id: user.id,
                login: user.login,
                type: user.type,
                accessToken: token
            });
        })
        .catch((error) => res.status(500).send({ message: error.message }));
    },
}