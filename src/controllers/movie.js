const sequelize = require('sequelize');
const Movie = require('../models').Movie;
const Op = sequelize.Op;

module.exports = {
    list(req, res) {
        const {name, description, director, genre} = req.query;
        
        const attr = [
            'id',
            'name',
            'description',
            'director',
            'genre',
            'updatedAt'
        ];

        Movie.findAll({
            attributes: attr.concat([
                [sequelize.literal(`(SELECT (SUM(v.vote)/COUNT(*)) FROM votes AS v WHERE v.movieId = Movie.id)`), 'votes']
            ]),
            where: {
                name: {
                    [Op.like]: `%${(name) ? name : ''}%`
                },
                description: {
                    [Op.like]: `%${(description) ? description : ''}%`
                },
                director: {
                    [Op.like]: `%${(director) ? director : ''}%`
                },
                genre: {
                    [Op.like]: `%${(genre) ? genre : ''}%`
                },
            }
        })
        .then((movies) => res.send(movies))
        .catch((error) => res.status(400).send(error));
    },
    async create(req, res) {

        const {name, description, director, genre} = req.body;

        return Movie.create({
            name: name,
            description: description,
            director: director,
            genre: genre,
            userId: req.userId
        })
        .then((movie) => res.status(201).send({
            id: movie.id,
            name: movie.name,
            description: movie.description,
            createdAt: movie.createdAt
        }))
        .catch((error) => res.status(400).send(error));
    },
}